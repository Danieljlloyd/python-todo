import shelve

class ShelveDatabase(object):
    def __init__(self, db_file):
        self.db_file = db_file

    def addTodo(self, todo):
        rc = None

        try:
            db = shelve.open(self.db_file)
            db['todos'] = db['todos'] + [todo]
            rc = todo
        except KeyError:
            db['todos'] = [todo]
        finally:
            db.close()

        return rc

    def getNumberOfTodos(self):
        n = 0

        try:
            db = shelve.open(self.db_file)
            n = len(db['todos'])
            db.close()
        except Exception:
            pass

        return n

    def retrieveById(self, ident):
        todo = None

        try:
            db = shelve.open(self.db_file)
            todo = db['todos'][ident-1]
        except KeyError:
            db['todos'] = []
        finally:
            db.close()

        return todo

    def updateTodo(self, ident, todoData):
        finalTodoData = None

        try:
            db = shelve.open(self.db_file)
            todos = db['todos']
            todos[ident-1] = todoData
            db['todos'] = todos
        except KeyError:
            db['todos'] = []
        finally:
            db.close()

        return finalTodoData

    def retrieveAllTodos(self):
        todos = []

        try:
            db = shelve.open(self.db_file)
            todos = [(i+1, todo) for i, todo in enumerate(db['todos'])]
        except KeyError:
            db['todos'] = []
        finally:
            db.close()

        if todos is None:
            todos = []

        return todos
