class MemoryDatabase(object):
    def __init__(self):
        self.todos = []

    def _getNewIdentifier(self):
        return len(self.todos)

    def addTodo(self, todo):
        self.todos.append(todo)

    def getNumberOfTodos(self):
        return len(self.todos)

    def retrieveById(self, ident):
        return self.todos[ident-1]

    def updateTodo(self, ident, todoData):
        self.todos[ident-1] = todoData
        return self.todos[ident-1]

    def retrieveAllTodos(self):
        return [(i+1, todo) for i, todo in enumerate(self.todos)]
