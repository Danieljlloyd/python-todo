import copy

class TodoData(object):
    def __init__(self, content, complete):
        self.content = content
        self.complete = complete

    def __eq__(self, other):
        return self.content == other.content and self.complete == other.complete

def todoDataFromTodo(todo):
    return TodoData(**todo.__dict__)

def todoFromTodoData(todoData):
    return Todo(**todoData.__dict__)

class Todo(object):
    def __init__(self, content, complete):
        self.content = content
        self.complete = complete

    def getContent(self):
        return self.content

    def updateContent(self, new_content):
        return Todo(
            content=new_content,
            complete=self.complete,
        )

    def isComplete(self):
        return self.complete

    def completeTodo(self):
        return Todo(
            content=self.content,
            complete=True,
        )

    def markIncomplete(self):
        return Todo(
            content=self.content,
            complete=False,
        )

class TodoList(object):
    def __init__(self, name, todos):
        self.name = name
        self.todos = todos

    def size(self):
        return len(self.todos)

    def addTodo(self, todo):
        return TodoList(
            name=self.name,
            todos=self.todos + [todo],
        )

    def getName(self):
        return self.name

def updateTodoContent(todoData, newContent):
    oldTodo = todoFromTodoData(todoData)
    return todoDataFromTodo(oldTodo.updateContent(newContent))

def completeTodo(todoData):
    oldTodo = todoFromTodoData(todoData)
    return todoDataFromTodo(oldTodo.completeTodo())

def markTodoIncomplete(todoData):
    oldTodo = todoFromTodoData(todoData)
    return todoDataFromTodo(oldTodo.markIncomplete())
