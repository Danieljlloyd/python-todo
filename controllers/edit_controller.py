from todo import updateTodoContent, completeTodo

class EditController(object):
    def __init__(self, db):
        self.db = db

    def updateTodoContent(self, ident, content):
        todoData = self.db.retrieveById(ident)
        todoData = updateTodoContent(todoData, content)
        self.db.updateTodo(ident, todoData)

    def completeTodo(self, ident):
        todoData = self.db.retrieveById(ident)
        todoData = completeTodo(todoData)
        self.db.updateTodo(ident, todoData)
