from todo import TodoData

class TodoCreationController(object):
    def __init__(self, db):
        self.db = db

    def createTodo(self, content):
        self.db.addTodo(TodoData(
            content=content,
            complete=False,
        ))
