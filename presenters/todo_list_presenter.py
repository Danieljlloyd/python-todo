class TodoListViewModel(object):
    def __init__(self, title, todos):
        self.title = title
        self.todos = todos

    def __eq__(self, other):
        return self.title == other.title and self.todos == other.todos

class TodoListPresenter(object):
    def __init__(self, db, title):
        self.db = db
        self.title = title

    def createPresentation(self):
        todoList = self.db.retrieveAllTodos()
        return TodoListViewModel(
            title=self.title,
            todos=todoList,
        )
