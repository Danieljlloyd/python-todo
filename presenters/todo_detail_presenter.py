class TodoDetailViewModel(object):
    def __init__(self, ident, content, complete):
        self.ident = ident
        self.content = content
        self.complete = complete

    def __eq__(self, other):
        return self.ident == other.ident \
            and self.content == other.content \
            and self.complete == other.complete

class TodoDetailPresenter(object):
    def __init__(self, db):
        self.db = db

    def createPresentation(self, ident):
        todo = self.db.retrieveById(ident)
        return TodoDetailViewModel(
            ident=ident,
            content=todo.content,
            complete=todo.complete,
        )
