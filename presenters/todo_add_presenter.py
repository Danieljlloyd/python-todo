class TodoAddViewModel(object):
    def __init__(self, title):
        self.title = title

    def __eq__(self, other):
        return self.title == other.title

class TodoAddPresenter(object):
    def __init__(self, db):
        self.db = db

    def createPresentation(self):
        return TodoAddViewModel(
            title='Add A New Todo'
        )
