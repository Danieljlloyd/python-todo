from views.base import View
from presenters.todo_add_presenter import TodoAddPresenter

class TodoAddView(View):
    def __init__(self, db):
        super(TodoAddView, self).__init__()
        self.presenter = TodoAddPresenter(db)

    def render(self):
        viewModel = self.presenter.createPresentation()
        template = self.env.get_template('todo_add.html')
        return template.render(model=viewModel)

if __name__ == '__main__':
    from database.memory_database import MemoryDatabase
    db = MemoryDatabase()
    todoAddView = TodoAddView(db)
    response = todoAddView.render()
    print response
