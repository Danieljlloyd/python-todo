from views.base import View
from presenters.todo_list_presenter import TodoListPresenter

class TodoListView(View):
    def __init__(self, db):
        super(TodoListView, self).__init__()
        title = 'My Todo List'
        self.presenter = TodoListPresenter(db, title)

    def render(self):
        viewModel = self.presenter.createPresentation()
        template = self.env.get_template('todo_list.html')
        return template.render(model=viewModel)

if __name__ == '__main__':
    from todo import TodoData
    from database.memory_database import MemoryDatabase
    db = MemoryDatabase()
    db.addTodo(TodoData(
        content='Do some thing',
        complete=False,
    ))
    db.addTodo(TodoData(
        content='Do another thing',
        complete=True,
    ))
    db.addTodo(TodoData(
        content='Do a third thing',
        complete=False,
    ))
    todoListView = TodoListView(db)
    response = todoListView.render()
    print response
