from jinja2 import Environment, FileSystemLoader, select_autoescape

class View(object):
    def __init__(self):
        self.env = Environment(
            loader=FileSystemLoader('/var/www/todo/templates'),
            autoescape=select_autoescape(['html', 'xml']),
        )

    def render(self):
        raise NotImplementedError()
