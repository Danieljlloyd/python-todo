from views.base import View
from presenters.todo_detail_presenter import TodoDetailPresenter

class TodoDetailView(View):
    def __init__(self, db):
        super(TodoDetailView, self).__init__()
        self.presenter = TodoDetailPresenter(db)

    def render(self, ident):
        viewModel = self.presenter.createPresentation(ident)
        template = self.env.get_template('todo_detail.html')
        return template.render(model=viewModel)

if __name__ == '__main__':
    from todo import TodoData
    from database.memory_database import MemoryDatabase
    db = MemoryDatabase()
    db.addTodo(TodoData(
        content='Do some thing',
        complete=False,
    ))
    db.addTodo(TodoData(
        content='Do another thing',
        complete=True,
    ))
    db.addTodo(TodoData(
        content='Do a third thing',
        complete=False,
    ))

    todoDetailView = TodoDetailView(db)
    response = todoDetailView.render(1)
    print response
