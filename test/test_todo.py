from unittest import TestCase
from todo import *

class TestTodo(TestCase):
    def testCreateTodo(self):
        todo = Todo('Something to do', False)
        self.assertEqual(todo.getContent(), 'Something to do')

    def testUpdateTodoContent(self):
        todo = Todo('Something to do', False)
        todo = todo.updateContent('A new thing')
        self.assertEqual(todo.getContent(), 'A new thing')

    def testCompleteTodo(self):
        todo = Todo('Something to do', False)
        self.assertFalse(todo.isComplete())

        todo = todo.completeTodo()
        self.assertTrue(todo.isComplete())

    def testMarkTodoIncomplete(self):
        todo = Todo('Something to do', True)
        self.assertTrue(todo.isComplete())

        todo = todo.markIncomplete()
        self.assertFalse(todo.isComplete())

class TestTodoList(TestCase):
    def testAddTodo(self):
        todoList = TodoList('My todo list', [])
        self.assertEqual(todoList.size(), 0)
        todoList = todoList.addTodo(Todo('Something to do', False))
        todoList = todoList.addTodo(Todo('Something else to do', False))
        self.assertEqual(todoList.size(), 2)

    def testName(self):
        todoList = TodoList('My todo list', [])
        self.assertEqual(todoList.getName(), 'My todo list')

class TestTodoEditActions(TestCase):
    def setUp(self):
        self.myTodo = TodoData(
            content='Something to do',
            complete=False,
        )

    def testTodoUpdateContent(self):
        newTodo = updateTodoContent(self.myTodo, 'New todo content')
        self.assertEqual(newTodo.content, 'New todo content')

    def testCompletionEditing(self):
        newTodo = completeTodo(self.myTodo)
        self.assertTrue(newTodo.complete)

        newTodo = markTodoIncomplete(newTodo)
        self.assertFalse(newTodo.complete)
