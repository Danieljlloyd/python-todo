from unittest import TestCase
from presenters.todo_add_presenter import TodoAddPresenter, TodoAddViewModel
from database.memory_database import MemoryDatabase

class TestTodoAddPresenter(TestCase):
    def testConstruction(self):
        db = MemoryDatabase
        presenter = TodoAddPresenter(db)

    def testCreatePresentation(self):
        db = MemoryDatabase
        presenter = TodoAddPresenter(db)
        viewModel = presenter.createPresentation()
        self.assertEqual(viewModel, TodoAddViewModel(
            title='Add A New Todo',
        ))
