from presenters.todo_list_presenter import TodoListPresenter, TodoListViewModel
from test.util import TodoTestCase
from database.memory_database import MemoryDatabase
from todo import TodoData

class TestPresentation(TodoTestCase):
    def setUp(self):
        self.todoOne = TodoData(
            content='Todo task one',
            complete=False,
        )

        self.todoTwo = TodoData(
            content='Todo task two',
            complete=False,
        )

        self.db = MemoryDatabase()

        self.db.addTodo(self.todoOne)
        self.db.addTodo(self.todoTwo)

        self.title = 'My Todo List'

        self.presenter = TodoListPresenter(self.db, self.title)

    def testCreatePresentation(self):
        presentation = self.presenter.createPresentation()

        self.assertEqual(presentation, TodoListViewModel(
            title=self.title,
            todos=[
                (1, self.todoOne),
                (2, self.todoTwo),
            ]
        ))
