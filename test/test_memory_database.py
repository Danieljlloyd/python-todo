from unittest import TestCase
from database.memory_database import MemoryDatabase
from todo import TodoData

class TestMemoryDatabase(TestCase):
    def setUp(self):
        self.db = MemoryDatabase()

    def testAddTodo(self):
        todoData = TodoData(
            content='Do something',
            complete=False,
        )

        self.db.addTodo(todoData)
        self.assertEqual(self.db.getNumberOfTodos(), 1)

        newTodo = TodoData(
            content='Another thing',
            complete=False,
        )

        self.db.addTodo(newTodo)
        self.assertEqual(self.db.getNumberOfTodos(), 2)

class TestsWithMockedData(TestCase):
    def setUp(self):
        self.db = MemoryDatabase()

        self.todoOne = TodoData(
            content='Do something',
            complete=False,
        )
        self.todoTwo = TodoData(
            content='Another thing',
            complete=False,
        )

        self.db.addTodo(self.todoOne)
        self.db.addTodo(self.todoTwo)

    def _assertEqualTodoData(self, todoOne, todoTwo):
        self.assertEqual(todoOne.content, todoTwo.content)
        self.assertEqual(todoOne.complete, todoTwo.complete)

    def testRetrieveTodoById(self):
        todoDb = self.db.retrieveById(1)
        self._assertEqualTodoData(todoDb, self.todoOne)

        todoDb = self.db.retrieveById(2)
        self._assertEqualTodoData(todoDb, self.todoTwo)

    def testUpdateTodo(self):
        updatedTodo = TodoData(
            content='Updated todo',
            complete=True,
        )

        todoDb = self.db.updateTodo(1, updatedTodo)
        self._assertEqualTodoData(updatedTodo, todoDb)

    def testRetrieveAllTodos(self):
        allTodos = self.db.retrieveAllTodos()
        self.assertEqual([
            (1, self.todoOne),
            (2, self.todoTwo),
        ], allTodos)
