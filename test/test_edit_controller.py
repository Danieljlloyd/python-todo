from test.util import TodoTestCase
from todo import TodoData
from database.memory_database import MemoryDatabase
from controllers.edit_controller import EditController

class TodoEditTests(TodoTestCase):
    def setUp(self):
        self.db = MemoryDatabase()
        self.db.addTodo(TodoData(
            content='Old todo content',
            complete=False,
        ))
        self.myController = EditController(self.db)

    def testCreateController(self):
        self.myController.updateTodoContent(1, 'New todo content')
        todoDb = self.db.retrieveById(1)
        self.assertEqual(todoDb.content, 'New todo content')

    def testCompleteTodo(self):
        self.myController.completeTodo(1)
        todoDb = self.db.retrieveById(1)
        self.assertTrue(todoDb.complete)
