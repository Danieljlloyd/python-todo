import unittest
from database.memory_database import MemoryDatabase
from controllers.todo_creation_controller import TodoCreationController
from todo import TodoData

class TestCreationController(unittest.TestCase):
    def testCreation(self):
        db = MemoryDatabase()
        controller = TodoCreationController(db)
        controller.createTodo('My first todo')
        self.assertEqual(db.getNumberOfTodos(), 1)
