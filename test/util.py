from unittest import TestCase

class TodoTestCase(TestCase):
    def _assertEqualTodoData(self, todoOne, todoTwo):
        self.assertEqual(todoOne.content, todoTwo.content)
        self.assertEqual(todoOne.complete, todoTwo.complete)
