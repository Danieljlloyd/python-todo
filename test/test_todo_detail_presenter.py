from unittest import TestCase
from presenters.todo_detail_presenter import TodoDetailPresenter, \
    TodoDetailViewModel
from database.memory_database import MemoryDatabase
from todo import TodoData

class TestTodoDetailPresenter(TestCase):
    def testCreation(self):
        db = MemoryDatabase()
        todoDetailPresenter = TodoDetailPresenter(db)

    def testCreatePresentation(self):
        todo = TodoData(
            content='My todo',
            complete=True,
        )
        db = MemoryDatabase()
        db.addTodo(todo)
        presenter = TodoDetailPresenter(db)
        viewModel = presenter.createPresentation(1)
        self.assertEqual(viewModel, TodoDetailViewModel(
            ident=1,
            content=todo.content,
            complete=todo.complete,
        ))
