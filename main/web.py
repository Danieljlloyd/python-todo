from flask import Flask, redirect, url_for, request
app = Flask(__name__)

from views.todo_list_view import TodoListView
from views.todo_detail_view import TodoDetailView
from views.todo_add_view import TodoAddView
from controllers.edit_controller import EditController
from controllers.todo_creation_controller import TodoCreationController
from database.shelve_database import ShelveDatabase

def createDatabase():
    return ShelveDatabase('/var/www/todo/todo.db')

@app.route('/')
def todo_list():
    db = createDatabase()
    view = TodoListView(db)
    return view.render()

@app.route('/todo_detail/<ident>', methods=['POST', 'GET'])
def todo_detail(ident):
    ident = int(ident)
    db = createDatabase()

    if request.method == 'GET':
        view = TodoDetailView(db)
        return view.render(int(ident))

    elif request.method == 'POST':
        controller = EditController(db)
        controller.updateTodoContent(ident, request.form['content'])

        if 'completed' in request.form and request.form['completed'] == 'on':
            controller.completeTodo(ident)

        return redirect(url_for('todo_list'))

@app.route('/new_todo', methods=['POST', 'GET'])
def todo_add():
    db = createDatabase()
    if request.method == 'GET':
        view = TodoAddView(db)
        return view.render()
    elif request.method == 'POST':
        controller = TodoCreationController(db)
        controller.createTodo(request.form['content'])
        return redirect(url_for('todo_list'))

if __name__ == '__main__':
    app.run(debug=True)
